[![pipeline status](https://gitlab.com/psnszsn/temperature-log-api/badges/master/pipeline.svg)](https://gitlab.com/psnszsn/temperature-log-api/commits/master)

# Temperature log API

Log the temeprature (or any other metric) of your server/PC by sending HTTP Requests to a REST Api, store the data in a SQLite database and display it using HighCharts.

You can see it working at <https://x.panazan.ro/temp>

Use a variation of this bash script to send relevant temperature readings to the server:

```
#!/bin/bash

temps=$(sensors | grep -e 'Package' | awk '{print $4}' | sed 's/[^0-9\.]//g')
temp1=$(echo -e $temps | cut -d$' ' -f1)
temp2=$(echo -e $temps | cut -d$' ' -f2)

temp3=$(sensors | grep -e 'Core' | awk '{print $3}' | sed 's/[^0-9\.]//g'|awk '{ total += $1; count++ } END { print total/count }')
temp4=$(smartctl -d ata -A /dev/sdb | grep Temperature_Celsius | awk '{print $10}')
temp5=0 #not used

curl -H "Content-Type: application/json" -X POST -d "{\"id\": 0,\"timestamp\": 0,\"temp1\": ${temp1}, \"temp2\": ${temp2}, \"temp3\": ${temp3}, \"temp4\": ${temp4}}" http://localhost:8000/temp/add/password

```