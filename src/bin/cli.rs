#[macro_use]
extern crate clap;
extern crate rusqlite;
extern crate time;

use clap::{App, SubCommand};
use rusqlite::Connection;
use time::Timespec;

#[derive(Debug)]
struct Temp {
    id: u32,
    timestamp: i64,
    temp1: Option<f64>,
    temp2: Option<f64>,
    temp3: Option<f64>,
    temp4: Option<f64>,
    temp5: Option<f64>,
}

fn add(
    _pat: &std::path::Path,
    conn: &Connection,
    temp1: f64,
    temp2: f64,
    temp3: f64,
    temp4: f64,
    temp5: f64,
) {
    let me = Temp {
        id: 0,
        timestamp: time::get_time().sec,
        temp1: Some(temp1),
        temp2: Some(temp2),
        temp3: Some(temp3),
        temp4: Some(temp4),
        temp5: Some(temp5),
    };

    match conn.execute("INSERT INTO temps (timestamp, temp1, temp2, temp3, temp4, temp5) VALUES (?1, ?2, ?3, ?4, ?5, ?6)", &[&me.timestamp, &me.temp1, &me.temp2, &me.temp3, &me.temp4, &me.temp5]){
        Ok(updated) => println!("{} rows were updated", updated),
        Err(err) => println!("update failed: {}", err),
    }
}

fn printall(_pat: &std::path::Path, conn: &Connection) {

    let mut stmt = conn
        .prepare("SELECT id, timestamp, temp1, temp2, temp3, temp4, temp5 FROM temps")
        .unwrap();
    let person_iter =
        stmt.query_map(&[], |row| Temp {
            id: row.get(0),
            timestamp: row.get(1),
            temp1: row.get(2),
            temp2: row.get(3),
            temp3: row.get(4),
            temp4: row.get(5),
            temp5: row.get(6),
        }).unwrap();

    for person in person_iter {
        println!("{:?}", person.unwrap());
    }
}

fn main() {
    let matches = App::new("temp-api")
        .version("0.1")
        .author("Vlad Panazan <brgdvz@gmail.com>")
        .about("Does awesome things")
        .subcommand(
            SubCommand::with_name("add")
                .about("adds row to db")
                .args_from_usage("<seq>... 'A sequence of whole positive numbers, i.e. 20 25 30'"),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("add") {
        let tt = values_t!(matches, "seq", f64).unwrap();
        println!("{:?}", tt);

        let _path = std::path::Path::new("./temps.sqlite");
        let conn = Connection::open(_path).unwrap();

        add(&_path, &conn, tt[0], tt[1], tt[2], tt[3], tt[4]);
        // printall(&_path, &conn);
    }

    
    // add(&_path, &conn, "sandel", 1.1, 1.1, 1.2, 1.2, 1.4);
    // printall(&_path, &conn);
}
