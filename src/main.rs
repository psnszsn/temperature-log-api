#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;
extern crate rusqlite;
extern crate time;

use rocket::State;
use rocket_contrib::{Json, Value};
use rusqlite::Connection;
use std::sync::Mutex;

#[derive(Debug, Serialize, Deserialize)]
struct Temp {
    id: u32,
    timestamp: i64,
    temp1: Option<f64>,
    temp2: Option<f64>,
    temp3: Option<f64>,
    temp4: Option<f64>,
    temp5: Option<f64>,
}

#[post("/add/<pass>", format = "application/json", data = "<message>")]
fn new(pass: String, message: Json<Temp>, conn: State<Mutex<Connection>>) -> Json<Value> {
    let conn = conn.lock().expect("map lock.");

    use std::fs::File;
    use std::io::prelude::*;

    let mut f = File::open("pass.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");


    if pass != contents {
        return Json(json!({
            "status": "error00"
        }));
    }

    if message.0.id != 0 {
        return Json(json!({
            "status": "error"
        }));
    }

    let me = Temp {
        id: 0,
        timestamp: time::get_time().sec,
        temp1: message.0.temp1,
        temp2: message.0.temp2,
        temp3: message.0.temp3,
        temp4: message.0.temp4,
        temp5: message.0.temp5,
    };

    match conn.execute("INSERT INTO temps (timestamp, temp1, temp2, temp3, temp4, temp5) VALUES (?1, ?2, ?3, ?4, ?5, ?6)", &[&me.timestamp, &me.temp1, &me.temp2, &me.temp3, &me.temp4, &me.temp5]){
        Ok(updated) => return Json(json!({ "status": "ok", "updated": updated })),
        Err(err) => return Json(json!({"status": "error", "error": format!("{}",err)})),
    }
}

#[get("/all")]
fn getall(conn: State<Mutex<Connection>>) -> Option<Json<Vec<Temp>>> {
    let conn = conn.lock().expect("map lock.");

    let mut stmt = conn
        .prepare("SELECT id, timestamp, temp1, temp2, temp3, temp4, temp5 FROM temps")
        .unwrap();
    let person_iter = stmt
        .query_map(&[], |row| Temp {
            id: row.get(0),
            timestamp: row.get(1),
            temp1: row.get(2),
            temp2: row.get(3),
            temp3: row.get(4),
            temp4: row.get(5),
            temp5: row.get(6),
        }).unwrap();

    let mut arrr = Vec::new();

    for person in person_iter {
        // println!("{:?}", person.unwrap());
        arrr.push(person.unwrap());
    }

    let retjson = Json(arrr);
    Some(retjson)
}

#[get("/avg")]
fn getavg(conn: State<Mutex<Connection>>) -> Option<Json<Vec<Temp>>> {
    let conn = conn.lock().expect("map lock.");

    let mut stmt = conn
        .prepare("SELECT id, MIN(timestamp), avg(temp1), avg(temp2), avg(temp3), avg(temp4), avg(temp5) FROM temps GROUP BY date(timestamp, 'unixepoch')")
        .unwrap();
    let person_iter = stmt
        .query_map(&[], |row| Temp {
            id: row.get(0),
            timestamp: row.get(1),
            temp1: row.get(2),
            temp2: row.get(3),
            temp3: row.get(4),
            temp4: row.get(5),
            temp5: row.get(6),
        }).unwrap();

    let mut arrr = Vec::new();

    for person in person_iter {
        // println!("{:?}", person.unwrap());
        arrr.push(person.unwrap());
    }

    let retjson = Json(arrr);
    Some(retjson)
}

#[get("/get/<id>", format = "application/json")]
fn get(id: usize, conn: State<Mutex<Connection>>) -> Option<Json> {
    // let conn = conn.lock().expect("map lock.");
    Some(Json(json!({
            "status": "nope"
        })))
}

#[get("/")]
fn home() -> rocket::response::NamedFile {
    rocket::response::NamedFile::open("./temp.html").unwrap()
}

#[catch(404)]
fn not_found() -> Json<Value> {
    Json(json!({
        "status": "error",
        "reason": "Resource was not found."
    }))
}

fn rocket() -> rocket::Rocket {
    let _path = std::path::Path::new("./temps.sqlite");
    rocket::ignite()
        .mount("/temp", routes![new, getall, getavg, home])
        .catch(catchers![not_found])
        .manage(Mutex::new(Connection::open(_path).unwrap()))
}

fn main() {
    rocket().launch();
}
